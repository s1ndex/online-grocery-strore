import React from "react";
import axios from "axios";

class restConnector {

    static async login(username, password) {
        axios.post("http://localhost:8080/j_spring_security_check?username="+username+"&password="+password, null, {
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {

                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
            .then(response => {
                if (response.status === 200 && response.data.loggedIn === undefined) {
                    const data = response.data;

                    localStorage.setItem('storeUserId', data.storeUserId);
                    localStorage.setItem('username', data.username);
                    localStorage.setItem('role', data.role);
                    localStorage.setItem('firstName', data.firstName);
                    localStorage.setItem('lastName', data.lastName);
                    localStorage.setItem('email', data.email);

                    alert("Hello, " + data.username)

                    console.log(response.data)
                    window.location.replace("http://localhost:3000/");
                } else {
                    alert("Bad credentials!")
                }
            })
    }




    static updatePass(pass) {
        const response = fetch("http://localhost:8080/clients/pass/" + localStorage.getItem("storeUserId") + "/" + pass, {
            method: 'PUT'
        }).then(response => {
            if (response.status === 200) {
                alert("Password changed successfully")
            }
        })
    }


    static pushItemToCart(productId, description, name, price, amount) {
        const response = fetch('http://localhost:8080/cart/items/'+localStorage.getItem("storeUserId"), {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                prodId: productId,
                description: description,
                name: name,
                price: price,
                amount: amount
            }),
        })
    }

    static deleteFromCart(prodId) {
        const response = fetch("http://localhost:8080/cart/items/"+localStorage.getItem("storeUserId") +"/" + prodId, {
            method: 'DELETE'
        })
        window.location.reload();
    }

    static deleteAllFromCart() {
        const response = fetch("http://localhost:8080/cart/items/all/"+localStorage.getItem("storeUserId") + "/", {
            method: 'DELETE'
        })
        console.log("http://localhost:8080/cart/items/all/"+localStorage.getItem("storeUserId"))
       alert("Thanks for purchase!")
        window.location.reload();
    }

    static getCart() {
        const response = fetch("http://localhost:8080/cart/"+localStorage.getItem("storeUserId"), {
            method: 'GET',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
        })
    }

    static register(username, email, password, name, lastName) {
        const response = fetch('http://localhost:8080/clients/registration', {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: name,
                lastName: lastName,
                email: email,
                password: password,
                username: username,
                role: 1,
                status: "true"
            }),
        })
            .then(response => {
            if (response.status === 201) {
                alert("Registration is successful!")
                window.location.replace("http://localhost:3000/");
            } else {
                alert("User with such email or username already exists!")
            }
        });
        console.log(response)
    }

    static logout() {
        fetch('http://localhost:8080/j_spring_security_logout').catch(console.log);
        localStorage.clear();
    }

    static isLoggedIn = () => {
        return !!localStorage.getItem('storeUserId')
    }

    static getRole = () => {
        return localStorage.getItem('role')
    }

}

export default restConnector;