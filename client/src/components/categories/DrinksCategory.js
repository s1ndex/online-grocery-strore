import React from "react";
import Item from "../Item";

class DrinksCategory extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("http://localhost:8080/categories/4")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div>
                    <h1 style={{paddingLeft: "20px", marginTop: "20px", marginBottom: "10px"}}>Drinks</h1>

                    <div className="app">

                        {/*<Item itemName='Packed Salad with tuna and egg' price='200' id='31'/>*/}
                        {/*<Item itemName='Meat' price='200' id='11'/>*/}
                        {items.map(item => (
                            <Item itemName={item.name} price={item.price} id={item.productId}/>
                        ))}
                    </div>
                </div>
            );
        }
    }

}

export default DrinksCategory;