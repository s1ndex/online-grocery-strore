import React from "react"
import "./Header.css"
import { Button } from 'reactstrap';
import {Link} from "react-router-dom";
import restConnector from "../../rest/restConnector";

class Header extends React.Component {

    constructor(props) {
        super(props);
    }

    logoutPressed = () => {
        restConnector.logout();
        window.location.reload(false);
    }

    renderNotLoggedIn = () => {
        return(
            <div className="header">
                <div style={{alignSelf: "center", width: "50%"}}>
                    <h3 style={{color: "white"}}>Grocery Store</h3>
                </div>

                <div className="dropdown">


                    <button className="header-button">
                        Profile
                        <div className="dropdown-content">

                            <Link to='/register'>
                                <a href="#">Register</a>
                            </Link>

                            <Link to='/login'>
                                <a href="#">Log in</a>
                            </Link>
                        </div>
                    </button>
                </div>
            </div>
        );
    }

    renderLoggedIn = () => {
        return(
            <div className="header">
                <div style={{alignSelf: "center", width: "50%"}}>
                    <h3 style={{color: "white"}}>Grocery Store</h3>
                </div>

                <div className="dropdown">

                    <Link to='/cart'>
                        <Button className="header-button">Cart</Button>
                    </Link>

                    <button className="header-button">
                        {localStorage.getItem('username')}
                        <div className="dropdown-content">

                            <Link to='/userInfo'>
                                <a href="#">To profile</a>
                            </Link>

                            <Link onClick={this.logoutPressed}>
                                <a href="#">Log out</a>
                            </Link>

                        </div>
                    </button>
                </div>
            </div>
        );
    }

    renderAdmin = () => {
        return(
            <div className="header">
                <div style={{alignSelf: "center", width: "50%"}}>
                    <h3 style={{color: "white"}}>Grocery Store</h3>
                </div>

                <div className="dropdown">

                    <Link to='/admin'>
                        <Button className="header-button">Users</Button>
                    </Link>

                    <button className="header-button">
                        {localStorage.getItem('username')}
                        <div className="dropdown-content">

                            <Link to='/userInfo'>
                                <a href="#">To profile</a>
                            </Link>

                            <Link onClick={this.logoutPressed}>
                                <a href="#">Log out</a>
                            </Link>

                        </div>
                    </button>
                </div>
            </div>
        );
    }

    render() {
        if (!restConnector.isLoggedIn()) return this.renderNotLoggedIn()
        else {
            return localStorage.getItem("role") === "ADMIN" ? this.renderAdmin() : this.renderLoggedIn()
        }

    }


}

export default Header;