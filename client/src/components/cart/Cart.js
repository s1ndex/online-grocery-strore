import React from "react";
import CartItem from "./cart-item/CartItem";
import './Cart.css'
import restConnector from "../../rest/restConnector";

class Cart extends React.Component {

    constructor(props) {
        super(props);
        this.price = 0;
        this.state = {
            error: null,
            isLoaded: false,
            price: 0,
            items: []
        };
    }

    componentDidMount() {
        if (!restConnector.isLoggedIn() || localStorage.getItem("role") === "ADMIN") window.location.replace("http://localhost:3000/");
        else {
            fetch("http://localhost:8080/cart/" + localStorage.getItem("storeUserId"), {
                method: 'GET',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
            })
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            isLoaded: true,
                            items: result.cartItems,
                            price: result.price
                        }
                        );
                        console.log(result.cartId)
                    },
                    (error) =>
                        this.setState( {
                            isLoaded: true, error
                        } )
                )
        }
    }


    render() {
        if (!restConnector.isLoggedIn() || localStorage.getItem("role") === "ADMIN") window.location.replace("http://localhost:3000/");
        const { error, isLoaded, items , price} = this.state;
        if (error) {
            return (
                <div>
                    <h1 style={{paddingLeft: "20px", marginTop: "20px", marginBottom: "10px"}}>Cart</h1>
                    <div>Error: {error.message}</div>
                </div>
                 )
        } else if (!isLoaded ) {
            return (
                <div>
                    <h1 style={{paddingLeft: "20px", marginTop: "20px", marginBottom: "10px"}}>Cart</h1>
                    <div>Loading</div>
                </div>
            )
        } else {
            return(
                <div>
                    <h1 style={{paddingLeft: "20px", marginTop: "20px", marginBottom: "10px"}}>Cart</h1>
                    <div className="cart-page">
                        <div className="cart">

                            {items.map(item => (
                                <CartItem name={item.name} price={item.price} id={item.prodId} prodId={item.productId}/>
                            ))}

                        </div>

                        <div className="totals">
                            <h3>Total: {this.state.price} CZK</h3>
                            <button onClick={this.handleDelete} className="submit-button">Submit</button>
                        </div>

                    </div>
                </div>
            );
        }
    }

    handleDelete = (event) => {
        event.preventDefault();
        restConnector.deleteAllFromCart();
    }

    priceCount() {

        var price = 0;
        return price;
    }

}

export default Cart;