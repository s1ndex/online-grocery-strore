import React from "react";
import './CartItem.css'
import restConnector from "../../../rest/restConnector";

class  CartItem extends React.Component {

    constructor(props) {
        super(props);
    }

    state = {
        prodId: this.props.prodId
    }


    render() {
        return(
            <div className="item-placed">
                <img src={process.env.PUBLIC_URL + "/img/" + this.props.id + ".jpg"} style={{width: "20%", height: "80%"}}/>
                <h5>{this.props.name}</h5>

                <div className="counter-block">

                </div>

                <h4>Price: {this.props.price}</h4>

                <button onClick={this.handleEvent} style={{width: "30px", height: "30px", borderStyle: 'none', backgroundColor: "#ededed", fontSize: "22px"}}>x</button>

            </div>
        );
    }

    handleEvent = (event) => {
        event.preventDefault();
        restConnector.deleteFromCart(this.state.prodId)

    }

}

export default CartItem;