import React from "react";
import restConnector from "../rest/restConnector";

class Item extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="item">
                <img src={process.env.PUBLIC_URL + "/img/" + this.props.id + ".jpg"} style={{width: "100%", height: "40%"}} />
                <h4 style={{alignSelf: "center", fontWeight: "500", height: "60px", marginLeft: "10px", marginRight: "10px", paddingTop:"10px", textAlign: "center"}}>{this.props.itemName}</h4>

                <h2 style={{alignSelf: "center", marginTop: "20px"}}>{this.props.price} Czk</h2>
                <button onClick={this.handle} style={{alignSelf: "center", width: "80%", height: "30px", marginBottom: "10px", marginTop: "10px", backgroundColor: "white", borderRadius: "12px", fontSize: "16px"}}>Add to Cart</button>
            </div>
        );
    }

    handle = (event) => {
       event.preventDefault();
       if (!restConnector.isLoggedIn() || localStorage.getItem("role") === "ADMIN") alert("Log in before purchasing!")
       else restConnector.pushItemToCart(this.props.id, "", this.props.itemName, this.props.price, 1)
    }

}



export default Item;