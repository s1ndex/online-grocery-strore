import React from "react";
import "./MainPage.css"
import CategoryBig from "./CategoryBig";
import {Link} from "react-router-dom";

class MainPage extends React.Component {

    render() {
        return(
            <div className="main-page-categories">
                <Link to='/categories/meat'>
                    <CategoryBig id={0} name="Meat"/>
                </Link>
                <Link to='/categories/bakery'>
                    <CategoryBig id={1} name="Bakery"/>
                </Link>
                <Link to='/categories/green'>
                    <CategoryBig id={2} name="Vegetables & fruits"/>
                </Link>
                <Link to='/categories/drink'>
                    <CategoryBig id={3} name="Drinks"/>
                </Link>
            </div>
        );
    }
}
export default MainPage;
