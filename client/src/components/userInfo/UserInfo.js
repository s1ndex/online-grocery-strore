import React from "react";
import './UserInfo.css'
import restConnector from "../../rest/restConnector";


class UserInfo extends React.Component {


    state = {
        password: "",
        submit: ""
    }

    componentDidMount() {
    }

    handleEvent = (event) => {
        event.preventDefault();
        if (this.state.password === this.state.submit)
            restConnector.updatePass(this.state.password)
        else
            alert("Passwords are not the same!")
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    renderInfo() {

        return(
            <div>
                <h1 style={{paddingLeft: "20px", marginTop: "20px", marginBottom: "10px"}}>User Info</h1>
                <div className='user-info'>
                    <div className="img-usr">
                        <img  src={process.env.PUBLIC_URL + "/img/user.png"} style={{width: "100%", height: "100%"}}/>
                    </div>
                    <form className="info-form">
                        <label>First Name</label>
                        <input type="text" placeholder={localStorage.getItem('firstName')} disabled={true}/>
                        <label>Second Name</label>
                        <input type="text" placeholder={localStorage.getItem('lastName')} disabled={true}/>
                        <label>E-mail</label>
                        <input type="text" placeholder={localStorage.getItem('email')} disabled={true}/>
                        <label>New Password</label>
                        <input onChange={this.handleChange} type="password" placeholder="New Password" name={"password"} value={this.state.password}/>
                        <label>Submit New Password</label>
                        <input onChange={this.handleChange} type="password" placeholder="Repeat Password" name={"submit"} value={this.state.submit}/>
                        <button onClick={this.handleEvent} className="submit-button">Submit</button>
                    </form>
                </div>
            </div>
        );
    }

    render() {
        if (!restConnector.isLoggedIn()) window.location.replace("http://localhost:3000/")
        else return this.renderInfo();
    }
}

export default UserInfo