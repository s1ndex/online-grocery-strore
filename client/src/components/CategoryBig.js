import React from "react";
import "./MainPage.css"

function CategoryBig({name, id}) {

    return(
        <div className="big-category">
            <img src={process.env.PUBLIC_URL + "/img/" + id + ".png"} style={{width: "50%", height: "40%"}}/>
            <h3>{name}</h3>
        </div>
    );
}

export default CategoryBig;