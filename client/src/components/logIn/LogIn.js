import React from "react";
import './LogIn.css'
import restConnector from "../../rest/restConnector";

class LogIn extends React.Component{

    state = {
        username: "",
        password: ""
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log(this.state.username)
        console.log(this.state.password)
        restConnector.login(this.state.username, this.state.password);
    }

    render() {
        if (restConnector.isLoggedIn()) window.location.replace("http://localhost:3000/")
        else return(
            <div>
                <h1 style={{paddingLeft: "20px", marginTop: "20px", marginBottom: "10px"}}>Log In</h1>
                <div className="login">
                    <form>
                        <input className="input-field" onChange={this.handleChange} name={"username"} id={"username"} value={this.state.username} type="text" placeholder="Username"/>
                    </form>

                    <form>
                        <input className="input-field" onChange={this.handleChange} name={"password"} id={"password"} value={this.state.password} type="password" placeholder="Password"/>
                    </form>

                    <button className="login-button" onClick={this.handleSubmit}>
                        Log In
                    </button>
                </div>
            </div>
        );
    }
}

export default LogIn;