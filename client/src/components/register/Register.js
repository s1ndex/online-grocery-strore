import React from "react";
import '../logIn/LogIn.css'
import restConnector from "../../rest/restConnector";

class Register extends React.Component{

    state = {
        username: "",
        email: "",
        password: "",
        name: "",
        lastName: ""
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log(this.state.username)
        console.log(this.state.email)
        console.log(this.state.password)
        restConnector.register(this.state.username, this.state.email, this.state.password, this.state.name, this.state.lastName);
    }

    render() {
        if (restConnector.isLoggedIn()) window.location.replace("http://localhost:3000/")
        else return(
            <div>
                <h1 style={{paddingLeft: "20px", marginTop: "20px", marginBottom: "10px"}}>Register</h1>
                <div className="login">
                    <form>
                        <input className="input-field" onChange={this.handleChange} name={"name"} id={"name"} value={this.state.name} type="text" placeholder="Name"/>
                    </form>

                    <form>
                        <input className="input-field" onChange={this.handleChange} name={"lastName"} id={"lastName"} value={this.state.lastName} type="text" placeholder="Last Name"/>
                    </form>
                    <form>
                        <input className="input-field" onChange={this.handleChange} name={"username"} id={"username"} value={this.state.username} type="text" placeholder="Username"/>
                    </form>

                    <form>
                        <input className="input-field" onChange={this.handleChange} name={"email"} id={"email"} value={this.state.email} type="text" placeholder="Email"/>
                    </form>

                    <form>
                        <input className="input-field" onChange={this.handleChange} name={"password"} id={"password"} value={this.state.password} type="password" placeholder="Password"/>
                    </form>


                    <form>
                        <input type="checkbox" value="Submit Password"/> I Agree with terms
                    </form>


                    <button className="login-button" onClick={this.handleSubmit}>
                        Register
                    </button>
                </div>
            </div>
        );
    }

    sendRegRequest() {

    }


}

export default Register