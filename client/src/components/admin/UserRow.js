import React from "react";


class UserRow extends React.Component {

    render() {
        return(
            <div className="user-row">
                <h5>{this.props.id}</h5>
                <h5>{this.props.name}</h5>
                <h5>{this.props.surname}</h5>
                <h5>{this.props.email}</h5>
                <h5>{this.props.role}</h5>
            </div>
        );
    }

}

export default UserRow;