import React from "react";
import './AdminPanel.css'
import UserRow from "./UserRow";
import Button from "reactstrap/es/Button";

class AdminPanel extends React.Component{

    state = {
        users: [],
        search: ""
    }

    handleAct = (event) => {
        event.preventDefault();
        if (this.state.search !== "") {
            fetch("http://localhost:8080/rest/search/name/" + this.state.search)
                .then(result => result.json())
                .then(result => {
                        this.setState({
                            users: result
                        })
                    },
                    error =>
                        this.setState( {
                            isLoaded: true, error
                        } ))
        } else {
            fetch("http://localhost:8080/rest/search/all")
                .then(result => result.json())
                .then(result => {
                        this.setState({
                            users: result
                        })
                    },
                    error =>
                        this.setState( {
                            isLoaded: true, error
                        } ))
        }
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    }

    componentDidMount() {
        if (!(localStorage.getItem("role") === "ADMIN")) {
            window.location.replace("http://localhost:3000/")
            alert("You don't have access for this page")
        }
        else {
            fetch("http://localhost:8080/rest/search/all")
                .then(result => result.json())
                .then(result => {
                    this.setState({
                        users: result
                    })
                },
                    error =>
                        this.setState( {
                            isLoaded: true, error
                        } ))
        }
    }

    render() {
        return(
            <div className="admin-panel">
                <h1 style={{paddingLeft: "20px", marginTop: "20px", marginBottom: "10px"}}>Users</h1>
                <div className="users">

                    <div className="button-form" style={{display: "flex", width: "500px", justifyContent: "space-around", alignItems: "baseline"}}>
                        <form style={{alignItems: "center"}}>
                            <input style={{width: "100%", height:"100%"}} type="text" placeholder="Name" onChange={this.handleChange} name={"search"} value={this.state.search} />
                        </form>
                        <button onClick={this.handleAct}>Find</button>
                    </div>

                    {
                        this.state.users.map(user => (
                            <UserRow id={user.storeUserId} email={user.email} name={user.lastName} surname={user.lastName} role={user.role}/>
                        ))
                    }
                </div>
            </div>
        );
    }
}

export default AdminPanel;