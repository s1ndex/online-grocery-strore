package com.eshop.groceryonline.utils;

public enum UserRole {
    ADMIN("ADMIN"),
    USER("USER"),
    BANNED("BANNED");

    private final String name;

    UserRole(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
