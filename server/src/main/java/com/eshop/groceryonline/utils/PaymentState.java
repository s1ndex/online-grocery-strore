package com.eshop.groceryonline.utils;

public enum PaymentState {
    PAYED("PAYED"),
    NOT_PAYED("NOT_PAYED"),
    CANCELLED("CANCELLED");

    private final String name;

    PaymentState(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
