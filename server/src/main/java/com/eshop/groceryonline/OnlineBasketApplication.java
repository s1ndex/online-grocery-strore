package com.eshop.groceryonline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableCaching
@EnableElasticsearchRepositories
public class OnlineBasketApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineBasketApplication.class, args);
    }

}
