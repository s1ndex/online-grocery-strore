package com.eshop.groceryonline.loaders;

import com.eshop.groceryonline.dao.StoreUserDao;
import com.eshop.groceryonline.dao.UserElasticRepo;
import com.eshop.groceryonline.entity.Client;
import com.eshop.groceryonline.entity.StoreUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class Loaders {

    private final ElasticsearchOperations operations;

    private final StoreUserDao storeUserDao;

    private final UserElasticRepo clientElasticRepo;

    @Autowired
    public Loaders(ElasticsearchOperations operations, StoreUserDao storeUserDao, UserElasticRepo clientElasticRepo) {
        this.operations = operations;
        this.storeUserDao = storeUserDao;
        this.clientElasticRepo = clientElasticRepo;
    }

    @PostConstruct
    @Transactional
    public void loadAll() {
        operations.putMapping(Client.class);
        List<StoreUser> list = storeUserDao.findAll();
        clientElasticRepo.saveAll(list);
    }
}
