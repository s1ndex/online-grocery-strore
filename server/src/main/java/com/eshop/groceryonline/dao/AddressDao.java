package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.Address;
import org.springframework.stereotype.Repository;

@Repository
public class AddressDao extends BaseDao<Address> {
    protected AddressDao() {
        super(Address.class);
    }
}
