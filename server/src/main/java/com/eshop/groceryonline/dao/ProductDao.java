package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.Product;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDao extends BaseDao<Product> {
    protected ProductDao() {
        super(Product.class);
    }
}
