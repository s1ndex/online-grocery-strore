package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.Category;
import com.eshop.groceryonline.entity.Product;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Objects;

@Repository
public class CategoryDao extends BaseDao<Category> {
    protected CategoryDao() {
        super(Category.class);
    }

    public void addOneProduct(Product product, Category category) {
        Objects.requireNonNull(product);
        Objects.requireNonNull(category);

        Collection<Product> products = category.getProducts();

        if (products.contains(product)) {
            return;
        }

        products.add(product);
        category.setProducts(products);
        persist(category);
    }
}
