package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.StoreUser;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class StoreUserDao extends BaseDao<StoreUser> {
    protected StoreUserDao() {
        super(StoreUser.class);
    }

    public StoreUser findByUsername(String username) {
        try {
            return entityManager
                    .createNamedQuery("StoreUser.findByUsername", StoreUser.class)
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
