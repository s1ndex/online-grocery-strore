package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.CartItem;
import org.springframework.stereotype.Repository;

@Repository
public class CartItemDao extends BaseDao<CartItem> {
    protected CartItemDao() {
        super(CartItem.class);
    }
}
