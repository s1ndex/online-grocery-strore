package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.Store;
import org.springframework.stereotype.Repository;

@Repository
public class StoreDao extends BaseDao<Store> {
    protected StoreDao() {
        super(Store.class);
    }
}
