package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.ClientOrder;
import org.springframework.stereotype.Repository;

@Repository
public class ClientOrderDao extends BaseDao<ClientOrder> {
    protected ClientOrderDao() {
        super(ClientOrder.class);
    }
}
