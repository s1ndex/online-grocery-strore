package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.Client;
import com.eshop.groceryonline.entity.ClientOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Repository
public class ClientDao extends BaseDao<Client> {
    protected ClientDao() {
        super(Client.class);
    }

    public void addOneClientOrder(Client client, ClientOrder order) {
        Objects.requireNonNull(client);
        Objects.requireNonNull(order);

        Collection<ClientOrder> clientOrders = client.getClientOrders();

        if (clientOrders.contains(order)) {
            return;
        }

        clientOrders.add(order);
        client.setClientOrders(clientOrders);
        persist(client);
    }

    public Client findByUsername(String username) {
        try {
            return entityManager
                    .createNamedQuery("Client.findByUsername", Client.class)
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Client findByEmail(String email) {
        try {
            return entityManager
                    .createNamedQuery("Client.findByEmail", Client.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
