package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.entity.Client;
import com.eshop.groceryonline.entity.StoreUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface UserElasticRepo extends ElasticsearchRepository<StoreUser, String> {
    List<StoreUser> findByFirstName(String text);
}
