package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.Cart;
import org.springframework.stereotype.Repository;

@Repository
public class CartDao extends BaseDao<Cart> {
    protected CartDao() {
        super(Cart.class);
    }
}
