package com.eshop.groceryonline.dao;

import com.eshop.groceryonline.dao.generic.BaseDao;
import com.eshop.groceryonline.entity.Administrator;
import org.springframework.stereotype.Repository;

@Repository
public class AdministratorDao extends BaseDao<Administrator> {
    protected AdministratorDao() {
        super(Administrator.class);
    }
}
