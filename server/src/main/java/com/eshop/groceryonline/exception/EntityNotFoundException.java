package com.eshop.groceryonline.exception;

public class EntityNotFoundException extends BaseException {

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public static EntityNotFoundException create(String resourceName, Object identifier) {
        return new EntityNotFoundException(resourceName + " identified by " + identifier + " not found.");
    }
}
