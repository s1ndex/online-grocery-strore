package com.eshop.groceryonline.exception;

public class ValidationException extends BaseException {
    public ValidationException(String message) {
        super(message);
    }
}
