package com.eshop.groceryonline.rest;

import com.eshop.groceryonline.entity.Category;
import com.eshop.groceryonline.entity.Product;
import com.eshop.groceryonline.service.CategoryService;
import com.eshop.groceryonline.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {
    private final ProductService productService;
    private final CategoryService categoryService;

    public ProductController(ProductService productService, CategoryService categoryService) {
        this.productService = productService;
        this.categoryService = categoryService;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getAllProducts() {
        return productService.findAll();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/create-product", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createNewProduct(@RequestBody Product product) {
        productService.persist(product);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/{productId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addProductToCategory(@PathVariable Integer productId, Category category) {
        Product product = new Product();

        if (productService.findById(productId) != null) {
            product = productService.findById(productId);
        }

        if (category.getProducts().contains(product)) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        categoryService.addOneProductToCategory(product, category);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
