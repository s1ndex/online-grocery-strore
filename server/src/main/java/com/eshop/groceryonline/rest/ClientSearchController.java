package com.eshop.groceryonline.rest;

import com.eshop.groceryonline.dao.UserElasticRepo;
import com.eshop.groceryonline.entity.Client;
import com.eshop.groceryonline.entity.StoreUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest/search")
public class ClientSearchController {

    private final UserElasticRepo clientElasticRepo;

    @Autowired
    public ClientSearchController(UserElasticRepo clientElasticRepo) {
        this.clientElasticRepo = clientElasticRepo;
    }

    @GetMapping(value = "/name/{text}")
    public List<StoreUser> searchName(@PathVariable final String text) {
        return clientElasticRepo.findByFirstName(text);
    }

    @GetMapping("/all")
    public List<StoreUser> findAll() {
        List<StoreUser> usersList = new ArrayList<>();
        Iterable<StoreUser> userses = clientElasticRepo.findAll();
        userses.forEach(usersList::add);
        return usersList;
    }
}
