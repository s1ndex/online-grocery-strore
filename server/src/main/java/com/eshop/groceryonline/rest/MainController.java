package com.eshop.groceryonline.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/main")
public class MainController {
    public ResponseEntity<String> returnSinglePageApp() {
        return new ResponseEntity<>("NSS project", HttpStatus.OK);
    }
}
