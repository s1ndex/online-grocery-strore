package com.eshop.groceryonline.rest;

import com.eshop.groceryonline.entity.Cart;
import com.eshop.groceryonline.entity.CartItem;
import com.eshop.groceryonline.entity.Client;
import com.eshop.groceryonline.rest.utils.RestUtils;
import com.eshop.groceryonline.security.model.UserDetails;
import com.eshop.groceryonline.service.CartItemService;
import com.eshop.groceryonline.service.CartService;
import com.eshop.groceryonline.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cart")
@PreAuthorize("permitAll()")
public class CartController {
    private final CartService cartService;
    private final ClientService clientService;
    private final CartItemService cartItemService;

    @Autowired
    public CartController(CartService cartService, ClientService clientService, CartItemService cartItemService) {
        this.cartService = cartService;
        this.clientService = clientService;
        this.cartItemService = cartItemService;
    }

    @GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCart(@PathVariable Integer id) {

        Client client = clientService.findByID(id);

        Cart cart = cartService.getCurrentClientCart(client);

        if (cart == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(cart, HttpStatus.OK);
    }

    @GetMapping(value = "/items", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCartItems() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final UserDetails details = (UserDetails) authentication.getPrincipal();

        Cart cart = cartService.getCurrentClientCart(clientService.findByID(details.getUser().getStoreUserId()));

        if (cart == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Map<String, String> map = new HashMap<>();
        cart.getCartItems().forEach((CartItem item) -> {
            map.put("CartItem ID: " + item.getProductId(),
                    RestUtils.createLocationHeaderFromCurrentUri("/{itemId}", item.getProductId()).getLocation().toString());
        });
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping(value = "/items/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createItem(@RequestBody CartItem cartItem, @PathVariable Integer id) {
        Client client = clientService.findByID(id);

        Cart cart = cartService.getCurrentClientCart(client);

        if (cart == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (cartItem == null || cartItem.getAmount() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            if (!checkRequiredData(cartItem)) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }

        Integer cartPrice = sumCartItemPrice(cartItem);

        cartPrice += cart.getPrice();
        cart.setPrice(cartPrice);
        cartItem.setCart(cart);
        //cartItemService.addCartItem(cartItem);
        cart.setPrice(cartPrice);
        cart.getCartItems().add(cartItem);
        cartService.update(cart);
        //cartItemService.updateCartItem(cartItem);

        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}",
                ((CartItem) (cart.getCartItems().toArray()[cart.getCartItems().size() - 1])).getProductId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    private boolean checkRequiredData(CartItem cartItem) {
        Integer cartItemPrice = cartItem.getPrice();
        if (cartItemPrice == null) {
            return false;
        }

        Integer amount = cartItem.getAmount();
        if (amount == null) {
            return false;
        }

        cartItem.setPrice(cartItemPrice);
        cartItem.setAmount(amount);

        return true;
    }

    private Integer sumCartItemPrice(CartItem cartItem) {
        return cartItem.getPrice() * cartItem.getAmount();
    }

    @PutMapping(value = "/items", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateItem(@RequestBody CartItem cartItem) {

        if (cartItem == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        CartItem dbRecord = cartItemService.findById(cartItem.getProductId());

        if (dbRecord == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Integer cartItemPriceBeforeUpdate = sumCartItemPrice(dbRecord);

        if (!checkRequiredData(cartItem)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final UserDetails details = (UserDetails) authentication.getPrincipal();

        Cart cart = cartService.getCurrentClientCart(clientService.findByID(details.getUser().getStoreUserId()));

        cartItemService.updateCartItem(dbRecord);

        cart.setPrice((cart.getPrice() - cartItemPriceBeforeUpdate) + sumCartItemPrice(cartItem));
        cartItem.setCart(cart);
        cartService.update(cart);
        cartItemService.updateCartItem(cartItem);

        return new ResponseEntity<>(RestUtils.createLocationHeaderFromCurrentUri(
                "/{id}", cartItem.getProductId()), HttpStatus.NO_CONTENT
        );
    }

    @DeleteMapping(value = "/items/{userId}/{itemId}")
    public ResponseEntity<?> removeCartItem(@PathVariable Integer itemId, @PathVariable Integer userId) {
        Client client = clientService.findByID(userId);

        Cart cart = cartService.getCurrentClientCart(client);

        if (cart == null) {
            System.out.println("huj");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        CartItem cartItem = cartItemService.findById(itemId);
        if (cartItem == null) {
            System.out.println("huj1");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            if (!cart.getCartItems().contains(cartItem)) {
                System.out.println("huj2");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                cart.getCartItems().remove(cartItem);
            }
        }

        cart.setPrice(cart.getPrice() - sumCartItemPrice(cartItem));
        cartItemService.removeCartItem(cartItem);
        cartService.update(cart);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/items/all/{userId}/")
    public ResponseEntity<?> removeAllItems(@PathVariable Integer userId) {
        Client client = clientService.findByID(userId);

        Cart cart = cartService.getCurrentClientCart(client);
        if (cart == null) {
            System.out.println("huj");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (cart.getCartItems().size() == 0) new ResponseEntity<>(HttpStatus.NO_CONTENT);

        List<CartItem> cartItemList = cart.getCartItems();

        for (CartItem cartItem : cartItemList) {
            if (cart.getCartItems().contains(cartItem)) {
                cart.getCartItems().remove(cartItem);
                cartItemService.removeCartItem(cartItem);
            }
        }

        cart.setPrice(0);
        cartService.update(cart);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/items/{itemId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCartContent(@PathVariable Integer itemId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final UserDetails details = (UserDetails) authentication.getPrincipal();

        Cart cart = cartService.getCurrentClientCart(clientService.findByID(details.getUser().getStoreUserId()));

        if (cart == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        CartItem cartItem = cartItemService.findById(itemId);
        if (cartItem == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            if (!cart.getCartItems().contains(cartItem)) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }

        cartItem.cartItemPrice = sumCartItemPrice(cartItem);
        return new ResponseEntity<>(cartItem, HttpStatus.OK);
    }

}
