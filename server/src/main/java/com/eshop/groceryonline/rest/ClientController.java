package com.eshop.groceryonline.rest;

import com.eshop.groceryonline.entity.Client;
import com.eshop.groceryonline.entity.ClientOrder;
import com.eshop.groceryonline.entity.StoreUser;
import com.eshop.groceryonline.exception.EntityNotFoundException;
import com.eshop.groceryonline.rest.utils.RestUtils;
import com.eshop.groceryonline.security.model.UserDetails;
import com.eshop.groceryonline.service.CartService;
import com.eshop.groceryonline.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/clients")
public class ClientController {

    private static final Logger LOG = LoggerFactory.getLogger(ClientController.class);
    private final ClientService clientService;
    private final CartService cartService;

    @Autowired
    public ClientController(ClientService clientService, CartService cartService) {
        this.clientService = clientService;
        this.cartService = cartService;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Client> getAllClients() {
        return clientService.findAll();
    }

    @PostMapping(value = "/registration", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> register(@RequestBody Client client) {
        if (clientService.findClientByUsername(client.getUsername()) != null
                || clientService.findClientByEmail(client.getEmail()) != null
        ) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        clientService.persist(client);
        cartService.persistEmptyCart(clientService.findClientByUsername(client.getUsername()));
        LOG.debug("User {} successfully registered.", client);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");

        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Client getClient(@PathVariable Integer id) {
        return clientService.findByID(id);
    }

    @PutMapping(value = "/pass/{id}/{pass}")
    public void updatePass(@PathVariable String pass, @PathVariable Integer id) {
        clientService.changePwd(id, pass);
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public StoreUser getCurrent() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(authentication.getPrincipal());
        final UserDetails details = (UserDetails) authentication.getPrincipal();
        System.out.println(details.getUser().getUsername());

        return details.getUser();
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<ClientOrder> getAllOrders() {
        StoreUser user = getCurrent();
        Client client = clientService.findByID(user.getStoreUserId());
        if (client == null) {
            throw EntityNotFoundException.create("Client", user.getStoreUserId());
        }

        return clientService.showClientOrders(client);
    }
}
