package com.eshop.groceryonline.rest;

import com.eshop.groceryonline.entity.Category;
import com.eshop.groceryonline.entity.Product;
import com.eshop.groceryonline.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@PreAuthorize("permitAll()")
@RestController
@RequestMapping("/categories")
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Product> getProductsByCategoryId(@PathVariable Integer id) {
        return categoryService.getAllProductsByCategoryId(id);
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Category> getCategory() {


        return categoryService.getAllCategories();
    }

}
