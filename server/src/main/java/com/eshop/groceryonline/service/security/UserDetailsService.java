package com.eshop.groceryonline.service.security;

import com.eshop.groceryonline.dao.StoreUserDao;
import com.eshop.groceryonline.entity.StoreUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final StoreUserDao storeUserDao;

    @Autowired
    public UserDetailsService(StoreUserDao storeUserDao) {
        this.storeUserDao = storeUserDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final StoreUser user = storeUserDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User with username " + username + " not found.");
        }
        return new com.eshop.groceryonline.security.model.UserDetails(user);
    }
}
