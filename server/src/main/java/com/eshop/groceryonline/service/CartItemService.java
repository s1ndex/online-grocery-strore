package com.eshop.groceryonline.service;

import com.eshop.groceryonline.dao.CartItemDao;
import com.eshop.groceryonline.entity.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CartItemService {

    private final CartItemDao cartItemDao;

    @Autowired
    public CartItemService(CartItemDao cartItemDao) {
        this.cartItemDao = cartItemDao;
    }

    public void addCartItem(CartItem cartItem) {
        cartItemDao.persist(cartItem);
    }

    public void updateCartItem(CartItem cartItem) {
        cartItemDao.update(cartItem);
    }

    public void removeCartItem(CartItem cartItem) {
        cartItemDao.remove(cartItem);
    }

    public void removeAll() {
        cartItemDao.removeAll();
    }

    public CartItem findById(Integer id) {
        return cartItemDao.find(id);
    }
}
