package com.eshop.groceryonline.service;

import com.eshop.groceryonline.dao.AddressDao;
import com.eshop.groceryonline.dao.AdministratorDao;
import com.eshop.groceryonline.dao.ClientDao;
import com.eshop.groceryonline.entity.Address;
import com.eshop.groceryonline.entity.Administrator;
import com.eshop.groceryonline.entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;


@Service
@Transactional
public class AdminService {
    private final AdministratorDao administratorDao;
    private final AddressDao addressDao;
    private final ClientDao clientDao;
    private final EntityManager entityManager;

    @Autowired
    public AdminService(AdministratorDao administratorDao, AddressDao addressDao, ClientDao clientDao, EntityManager entityManager) {
        this.administratorDao = administratorDao;
        this.addressDao = addressDao;
        this.clientDao = clientDao;
        this.entityManager = entityManager;
    }

    public void banUser(Client clientEntity) {
        Objects.requireNonNull(clientEntity);
        clientEntity.setStatus(false);
        clientDao.update(clientEntity);
    }

    public void changeUserHomeAddress(Client client, Address address) {
        Objects.requireNonNull(client);
        Objects.requireNonNull(address);

        if (!addressDao.exists(address.getAddressId())) {
            addressDao.persist(address);
        }

        Address old = client.getAddress();
        if (old != null) {
            old.setStoreUser(null);
            client.setAddress(null);
        }

        if (old != null && addressDao.exists(old.getAddressId()) && old.getStoreUser() == null) {
            addressDao.remove(old);
        }

        client.setAddress(address);
        address.setStoreUser(client);
        clientDao.update(client);
        addressDao.update(address);
    }

    public List<Administrator> findAllAdmins() {
        return administratorDao.findAll();
    }
}
