package com.eshop.groceryonline.service;

import com.eshop.groceryonline.dao.ProductDao;
import com.eshop.groceryonline.entity.Product;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class ProductService {
    private final ProductDao productDao;

    public ProductService(ProductDao productDao) {
        this.productDao = productDao;
    }

    public List<Product> findAll() {
        return productDao.findAll();
    }

    public Product findById(Integer id) {
        return productDao.find(id);
    }

    public void persist(Product product) {
        Objects.requireNonNull(product);

        if (product.getPrice() == null) {
            product.setPrice(0);
        }

        productDao.persist(product);
    }
}
