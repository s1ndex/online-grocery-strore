package com.eshop.groceryonline.service;

import com.eshop.groceryonline.dao.CartDao;
import com.eshop.groceryonline.dao.ClientDao;
import com.eshop.groceryonline.entity.Cart;
import com.eshop.groceryonline.entity.Client;
import com.eshop.groceryonline.utils.PaymentState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CartService {

    private final CartDao cartDao;
    private final ClientDao clientDao;

    @Autowired
    public CartService(CartDao cartDao, ClientDao clientDao) {
        this.cartDao = cartDao;
        this.clientDao = clientDao;
    }

    @Transactional
    public List<Cart> getAllCarts() {
        return cartDao.findAll();
    }

    @Transactional
    public boolean persistEmptyCart(Client client) {
        Cart cart = new Cart();
        cart.setClient(client);
        cart.setPrice(0);
        cart.setPaymentState(PaymentState.NOT_PAYED);
        cartDao.persist(cart);
        client.setCart(cart);
        clientDao.update(client);

        return true;
    }

    @Transactional
    public Cart getCurrentClientCart(Client client) {
        if (client == null) {
            return null;
        }

        if (client.getCart() == null) {
            persistEmptyCart(client);
        }

        return client.getCart();
    }

    @Transactional
    public boolean addNewCartForExistingClient(Client client) {
        if (client.getStoreUserId() == null) {
            return false;
        }

        return persistEmptyCart(client);
    }

    @Transactional
    public boolean changeCartPaymentState(Cart cart, PaymentState state) {
        if (cart.getPaymentState().equals(PaymentState.NOT_PAYED)) {
            cart.setPaymentState(state);
            cartDao.update(cart);
            return true;
        }

        return false;
    }

    public void update(Cart cart) {
        cartDao.update(cart);
    }
}
