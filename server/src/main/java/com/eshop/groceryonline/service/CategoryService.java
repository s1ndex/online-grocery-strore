package com.eshop.groceryonline.service;

import com.eshop.groceryonline.dao.CategoryDao;
import com.eshop.groceryonline.dao.ProductDao;
import com.eshop.groceryonline.entity.Category;
import com.eshop.groceryonline.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class CategoryService {

    private final CategoryDao categoryDao;
    private final ProductDao productDao;

    @Autowired
    public CategoryService(CategoryDao categoryDao, ProductDao productDao) {
        this.categoryDao = categoryDao;
        this.productDao = productDao;
    }

    public Collection<Product> getAllProductsByCategoryId(Integer id) {
        Category category = categoryDao.find(id);
        return category.getProducts();
    }

    @Cacheable(cacheNames = {"storeCache"})
    public List<Category> getAllCategories() {
        return categoryDao.findAll();
    }

    public void addOneProductToCategory(Product product, Category category) {
        categoryDao.addOneProduct(product, category);
        product.setCategory(category);
        productDao.update(product);
    }
}
