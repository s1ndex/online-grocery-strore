package com.eshop.groceryonline.service;

import com.eshop.groceryonline.dao.StoreUserDao;
import com.eshop.groceryonline.entity.StoreUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
@Transactional
public class StoreUserService {

    private final StoreUserDao storeUserDao;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public StoreUserService(StoreUserDao storeUserDao, PasswordEncoder passwordEncoder) {
        this.storeUserDao = storeUserDao;
        this.passwordEncoder = passwordEncoder;
    }

    public void persist(StoreUser storeUser) {
        Objects.requireNonNull(storeUser);
        storeUser.encodePassword(passwordEncoder);

        storeUserDao.persist(storeUser);
    }

}
