package com.eshop.groceryonline.service;

import com.eshop.groceryonline.dao.ClientDao;
import com.eshop.groceryonline.dao.StoreUserDao;
import com.eshop.groceryonline.entity.Address;
import com.eshop.groceryonline.entity.Client;
import com.eshop.groceryonline.entity.ClientOrder;
import com.eshop.groceryonline.entity.StoreUser;
import com.eshop.groceryonline.security.SecurityUtils;
import com.eshop.groceryonline.utils.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class ClientService {

    private final StoreUserDao storeUserDao;
    private final AdminService adminService;
    private final ClientDao clientDao;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ClientService(ClientDao clientDao, StoreUserDao storeUserDao, AdminService adminService, PasswordEncoder passwordEncoder) {
        this.storeUserDao = storeUserDao;
        this.adminService = adminService;
        this.clientDao = clientDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public Client findByID(Integer id) {
        return clientDao.find(id);
    }

    @Transactional
    public Client getCurrentClient() {
        StoreUser storeUser = SecurityUtils.getCurrentUser();

        if (storeUser == null) {
            return null;
        }

        return clientDao.find(storeUserDao.findByUsername(storeUser.getUsername()).getStoreUserId());
    }

    @Transactional
    public boolean alreadyExists(Client client) {
        return clientDao.findAll().contains(client);
    }

    @Transactional
    public void changeEmail(Client clientEntity, String email) {
        StoreUser entity = storeUserDao.find(clientEntity.getStoreUserId());
        entity.setEmail(email);
        storeUserDao.update(entity);
    }

    @Transactional
    public void changePwd(Integer client, String pwd) {
        StoreUser entity = storeUserDao.find(client);
        if (entity == null) return;
        entity.setPassword(pwd);
        entity.encodePassword(passwordEncoder);
        storeUserDao.update(entity);
    }

    @Transactional
    public Collection<ClientOrder> showClientOrders(Client user) {
        return user.getClientOrders();
    }

    @Transactional
    public void setNewAddress(Client client, Address address) {
        adminService.changeUserHomeAddress(client, address);
    }

    @Transactional
    public List<Client> findAll() {
        return clientDao.findAll();
    }

    @Transactional
    public void persist(Client client) {
        Objects.requireNonNull(client);
        client.encodePassword(passwordEncoder);

        if (client.getRole() == null) {
            client.setRole(UserRole.USER);
        }

        clientDao.persist(client);
    }

    @Transactional
    public Integer findIdByUsername(String username) {
        return storeUserDao.findByUsername(username).getStoreUserId();
    }

    @Transactional
    public Client findClientByUsername(String username) {
        return clientDao.findByUsername(username);
    }

    @Transactional
    public Client findClientByEmail(String email) {
        return clientDao.findByEmail(email);
    }
}
