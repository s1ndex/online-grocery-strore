package com.eshop.groceryonline.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "client", schema = "nss")
@PrimaryKeyJoinColumn(name = "client_id")
@NamedQueries({
        @NamedQuery(
                name = "Client.findByUsername",
                query = "SELECT c FROM Client c WHERE c.username = :username"
        ),
        @NamedQuery(
                name = "Client.findByEmail",
                query = "SELECT c FROM Client c WHERE c.email = :email"
        )
})
public class Client extends StoreUser {

    @Basic
    @Column
    private boolean status;
    public boolean getStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }


    @OneToMany(mappedBy = "clientByClientId")
    private Collection<ClientOrder> clientOrders = new ArrayList<>();
    public Collection<ClientOrder> getClientOrders() {
        return clientOrders;
    }
    public void setClientOrders(Collection<ClientOrder> clientOrdersByClientId) {
        this.clientOrders = clientOrdersByClientId;
    }

    @OneToOne
    private Cart cart;
    public Cart getCart() {
        return cart;
    }
    public void setCart(Cart cart) {
        this.cart = cart;
    }
}
