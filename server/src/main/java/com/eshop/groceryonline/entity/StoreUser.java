package com.eshop.groceryonline.entity;

import com.eshop.groceryonline.utils.UserRole;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;

@Entity
@Table(name = "store_user", schema = "nss")
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({
        @NamedQuery(
                name = "StoreUser.findByUsername",
                query = "SELECT u FROM StoreUser u WHERE u.username = :username"
        )
})
@Document(indexName = "users", type = "users")
public class StoreUser {

    @Id
    @GeneratedValue
    @Column
    @org.springframework.data.annotation.Id
    private Integer storeUserId;
    public Integer getStoreUserId() {
        return storeUserId;
    }
    public void setStoreUserId(Integer storeUserId) {
        this.storeUserId = storeUserId;
    }

    @Basic
    @Column
    private String firstName;
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column
    private String lastName;
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column
    private String username;
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column
    private String email;
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column
    private String password;
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    @OneToOne
    private Address address;
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }

    public StoreUser() {

    }

    private UserRole role;
    public UserRole getRole() {
        return role;
    }
    public void setRole(UserRole role) {
        this.role = role;
    }

    public void erasePassword() {
        this.password = null;
    }
    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }
}
