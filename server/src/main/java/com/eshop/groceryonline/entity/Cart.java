package com.eshop.groceryonline.entity;

import com.eshop.groceryonline.utils.PaymentState;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "cart", schema = "nss")
public class Cart {

    @Id
    @GeneratedValue
    @Column(name = "cart_id", nullable = false)
    private Integer cartId;
    public Integer getCartId() {
        return cartId;
    }
    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    @Basic
    @Column(name = "payment_state", nullable = true, length = 10)
    private PaymentState paymentState;
    public PaymentState getPaymentState() {
        return paymentState;
    }
    public void setPaymentState(PaymentState paymentState) {
        this.paymentState = paymentState;
    }

    @Basic
    @Column(name = "price", nullable = true, precision = 2)
    private Integer price;
    public Integer getPrice() {
        return price;
    }
    public void setPrice(Integer price) {
        this.price = price;
    }

    @OneToMany(mappedBy = "cartByCartId")
    private List<ClientOrder> clientOrders = new ArrayList<>();
    public List<ClientOrder> getClientOrders() {
        return clientOrders;
    }
    public void setClientOrders(List<ClientOrder> clientOrders) {
        this.clientOrders = clientOrders;
    }

//    @JsonManagedReference
    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL)
    private List<CartItem> cartItems = new ArrayList<>();
    public List<CartItem> getCartItems() {
        return cartItems;
    }
    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    @OneToOne
    @JsonIgnore
    private Client client;
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }
}
