package com.eshop.groceryonline.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "address", schema = "nss")
public class Address {

    @Id
    @GeneratedValue
    @Column(name = "address_id", nullable = false)
    private Integer addressId;
    public Integer getAddressId() {
        return addressId;
    }
    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "postcode", nullable = true, length = 10)
    private String postcode;
    public String getPostcode() {
        return postcode;
    }
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Basic
    @Column(name = "country", nullable = true, length = 50)
    private String country;
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "city", nullable = true, length = 50)
    private String city;
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "street", nullable = true, length = 50)
    private String street;
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "building", nullable = true, length = 10)
    private String building;
    public String getBuilding() {
        return building;
    }
    public void setBuilding(String building) {
        this.building = building;
    }

    @Basic
    @Column(name = "apartment", nullable = true, length = 10)
    private String apartment;
    public String getApartment() {
        return apartment;
    }
    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    @JsonIgnore
    @OneToOne
    private StoreUser storeUser;
    public StoreUser getStoreUser() {
        return storeUser;
    }
    public void setStoreUser(StoreUser storeUser) {
        this.storeUser = storeUser;
    }
}
