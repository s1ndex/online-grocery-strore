package com.eshop.groceryonline.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "client_order", schema = "nss")
public class ClientOrder {

    @Id
    @GeneratedValue
    @Column(name = "client_order_id", nullable = false)
    private Integer clientOrderId;
    public Integer getClientOrderId() {
        return clientOrderId;
    }
    public void setClientOrderId(Integer clientOrderId) {
        this.clientOrderId = clientOrderId;
    }

    @Basic
    @JsonIgnore
    @Column(name = "client_id", nullable = true)
    private Integer clientId;
    public Integer getClientId() {
        return clientId;
    }
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    @Basic
    @JsonIgnore
    @Column(name = "cart_id", nullable = true)
    private Integer cartId;
    public Integer getCartId() {
        return cartId;
    }
    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "client_by_client_id", referencedColumnName = "client_id")
    private Client clientByClientId;
    public Client getClientByClientId() {
        return clientByClientId;
    }
    public void setClientByClientId(Client clientByClientId) {
        this.clientByClientId = clientByClientId;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "cart_by_cart_id", referencedColumnName = "cart_id")
    private Cart cartByCartId;
    public Cart getCartByCartId() {
        return cartByCartId;
    }
    public void setCartByCartId(Cart cartByCartId) {
        this.cartByCartId = cartByCartId;
    }
}
