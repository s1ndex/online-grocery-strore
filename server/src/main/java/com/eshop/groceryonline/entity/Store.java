package com.eshop.groceryonline.entity;

import javax.persistence.*;

@Entity
@Table(name = "store", schema = "nss")
public class Store {

    @Id
    @GeneratedValue
    @Column(name = "store_id", nullable = false)
    private Integer storeId;
    public Integer getStoreId() {
        return storeId;
    }
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 100)
    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 100)
    private String address;
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
}
