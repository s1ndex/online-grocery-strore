package com.eshop.groceryonline.entity;

import javax.persistence.*;

@Entity
@Table(name = "administrator", schema = "nss")
@PrimaryKeyJoinColumn(name = "administrator_id")
public class Administrator extends StoreUser {

    @Basic
    @Column
    private boolean status;
    public boolean getStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
}
